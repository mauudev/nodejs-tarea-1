const Videogame = require('../models/videogame.model.js');

exports.create = (req, res) => {
    // Validate request
    if(!req.body.name) {
        return res.status(400).send({
            message: "Videogame amam can not be empty"
        });
    }

    const videogame = new Videogame({
        name: req.body.name || "No name", 
        board: req.body.board || "No board", 
        user: req.body.user || "No user"
    });

    videogame.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Videogame."
        });
    });
};

exports.findAll = (req, res) => {
    Videogame.find()
    .then(videogames => {
        res.send(videogames);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving videogames."
        });
    });
};

exports.findOne = (req, res) => {
    Videogame.findById(req.params.videogameId)
    .then(videogame => {
        if(!videogame) {
            return res.status(404).send({
                message: "Videogame not found with id " + req.params.videogameId
            });            
        }
        res.send(videogame);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Videogame not found with id " + req.params.videogameId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving videogame with id " + req.params.videogameId
        });
    });
};


exports.update = (req, res) => {
    // Validate Request
    if(!req.body.name) {
        return res.status(400).send({
            message: "Videogame name can not be empty"
        });
    }

    Videogame.findByIdAndUpdate(req.params.videogameId, {
        name: req.body.name || "No name", 
        board: req.body.board || "No board", 
        user: req.body.user || "No user"
    }, {new: true})
    .then(videogame => {
        if(!videogame) {
            return res.status(404).send({
                message: "Videogame not found with id " + req.params.videogameId
            });
        }
        res.send(videogame);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Videogame not found with id " + req.params.videogameId
            });                
        }
        return res.status(500).send({
            message: "Error updating videogame with id " + req.params.videogameId
        });
    });
};

exports.delete = (req, res) => {
    Videogame.findByIdAndRemove(req.params.videogameId)
    .then(videogame => {
        if(!videogame) {
            return res.status(404).send({
                message: "Videogame not found with id " + req.params.videogameId
            });
        }
        res.send({message: "Videogame deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Videogame not found with id " + req.params.videogameId
            });                
        }
        return res.status(500).send({
            message: "Could not delete videogame with id " + req.params.videogameId
        });
    });
};

