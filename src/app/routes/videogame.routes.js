module.exports = (app) => {
    const videogame = require('../controllers/videogame.controller.js');

    // Create a new videogame
    app.post('/videogame', videogame.create);

    // Retrieve all videogame
    app.get('/videogame', videogame.findAll);

    // Retrieve a single videogame with videogameId
    app.get('/videogame/:videogameId', videogame.findOne);

    // Update a videogame with videogameId
    app.put('/videogame/:videogameId', videogame.update);

    // Delete a videogame with videogameId
    app.delete('/videogame/:videogameId', videogame.delete);
}