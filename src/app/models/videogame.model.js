const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const VideogameSchema = mongoose.Schema({
    name: String,
    board: String,
    user: { type: Schema.ObjectId, ref: "User" } 
}, {
    timestamps: true
});

module.exports = mongoose.model('Videogame', VideogameSchema);
