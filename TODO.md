## TODOS
1. Agregar la libreria Babel a la aplicacion
2. Agregar Nodemon a la aplicacion
3. Sustituir import/export por require
4. Configurar eslint
5. Implementar Authentication de usuarios con Passport.js
6. Implementar Autorizaciones de usuarios con Passport.js
7. Usar las caracteristicas de async/await en las peticiones de el API
8. Reorganizacion de la estructura de archivos de la aplicacion
9. Implementar pruebas para usuarios
10. Implementar pruebas para videogames